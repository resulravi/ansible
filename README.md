## System Setup For Linux User Using Ansible Playbook

#### Prerequirements:

a. Ubuntu System with Ansible

#### Install Ansible
```
sudo apt update
sudo apt install ansible
```

#### Clone the Git repository
```
git clone https://gitlab.com/resulravi/ansible.git
```

#### Configuration Settings

```
vim ansible.cfg 

   inventory      = $ansible Repository clone Path
   Eg:inventory      = /home/resul/Music/ansible/hosts

vim hosts
   [all]
   Remote _ip_addres
   Eg:
   [all]
   192.168.1.12

vim credentials.yml
   remote_user: 'user_name'  
   remote_dest: '/home/user_name'
   
    //change remote user_name and destination
    Eg:remote_user: 'resul'  
       remote_dest: '/home/resul'

```
#### For run the playbook
```
cd /ansible

ssh-copy-id -i /home/$user/.ssh/id_rsa.pub remote_user@ip_address 
(copy your pub key to the remote system)

ansible-playbook master.yml -K
(K for giving the remote system password)
```



